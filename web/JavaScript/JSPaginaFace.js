/*Functions for use into PaginaFace.html*/

function cargar()
{
    var url="ButtonFacebook.html";
    $.ajax({   
        type: "POST",
        url:url,
        data:{},
        success: function(datos)
        {       
            $('.button-facebook').html(datos);
        }
    });
}
            
function getParameter(parametro)
{
    var result = "En ";
    var url = location.href;
    var index = url.indexOf("?");
    index = url.indexOf(parametro,index) + parametro.length;
                
    if (url.charAt(index) == "=")
    {
        var urlCadena = url.toString();
        var fin = url.indexOf("/",index);
                    
        var nomCadenaUrl=urlCadena.substring(index+1,fin);
        var porciones = nomCadenaUrl.split("%20");
                    
        var i;
                    
        for(i=0; i<porciones.length; i++)
        {    
            result = result.concat(porciones[i]+" ");
        }    
                    
    }
                 
    $(".result").val(result);
                
} 