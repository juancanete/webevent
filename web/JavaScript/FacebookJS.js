/*Javascript to manage Facebook's functions*/

var token;
var users = "";

window.fbAsyncInit = function() 
{
        FB.init({
        appId      : '196009357252881', // App ID
        channelUrl : '../EventPage/channel.html', // Channel File
        status     : true, // check login status
        cookie     : true, // enable cookies to allow the server to access the session
        xfbml      : true  // parse XFBML
        });


        FB.Event.subscribe('auth.authResponseChange', function(response) {

            if (response.status === 'connected') {
              testAPI();
            } else if (response.status === 'not_authorized') {
              FB.login();
            } else {
              FB.login();
            }
      });
  };

  // Load the SDK asynchronously
  (function(d){
   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement('script'); js.id = id; js.async = true;
   js.src = "//connect.facebook.net/en_US/all.js";
   ref.parentNode.insertBefore(js, ref);
  }(document));

  // Here we run a very simple test of the Graph API after login is successful. 
  // This testAPI() function is only called in those cases. 
  function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
          console.log('Good to see you, ' + response.name + '.');
        });
};

//Login on Facebook
function Login()
{

    FB.login(function(response) 
    {
       if (response.authResponse) 
       {
           token = FB.getAccessToken();
            //getUserInfo();
            getFriends(); 
           
       } 
       else 
       {
            console.log('User cancelled login or did not fully authorize.');
       }
     },{scope: 'create_event,user_events,email,user_photos,user_videos'});
}


function getUserInfo() 
{
     FB.api('/me', function(response) 
     {
         var str="<b>Name</b> : "+response.name+"<br>";
         str +="<b>Link: </b>"+response.link+"<br>";
         str +="<b>Username:</b> "+response.username+"<br>";
         str +="<b>id: </b>"+response.id+"<br>";
         str +="<b>Email:</b> "+response.email+"<br>";
         str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
         str +="<input type='button' value='Logout' onclick='Logout();'/>";
         str +="<input type='button' value='Get Friends' onclick='getFriends();'/>";
         document.getElementById("status").innerHTML=str;

    });
}


function getPhoto()
{
     FB.api('/me/picture?type=normal', function(response) 
     {
        var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
        document.getElementById("status").innerHTML+=str;

     });
}

function Logout()
{
    FB.logout(function(){document.location.reload();});
}


// Load the SDK asynchronously
(function(d)
{
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];

     if (d.getElementById(id)) 
     {
        return;
     }

     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
}
(document));

function getFriends() 
{

    FB.api('/me/friends', function(response) 
    {
        var listaAmigos = ("<div class=\"friends\"><ul id=\"elements\">");

            $.each(response.data,function(index,friend) 
            {
                if(index % 2 == 0) {
                    listaAmigos =listaAmigos.concat( "<li id =\""+ friend.id + 
                            "\" class=\"element-li1\"><a onclick=\"insertUser('"+ friend.id + 
                            "','"+ friend.name+"');\">" + friend.name + "</a> </li>");
                }else
                {
                    listaAmigos =listaAmigos.concat( "<li id =\""+ friend.id + 
                            "\" class=\"element-li2\"><a onclick=\"insertUser('"+ friend.id + 
                            "','"+ friend.name+"');\">" + friend.name + "</a> </li>");
                }
            });


        listaAmigos =listaAmigos.concat("</ul></div>");
        
        //Modify web page to insert correct button
        $("#lookfor-event").html("<a class=\"enlace\" onclick=\"create_event('" + 
                $("#nameEvent").val() + "','" + users + "','" + $("#description").val() + 
                "','" + $("#venue").val() + "','" + $("#date-hour").val() + 
                "');\">Crear Evento</a>");
        $(".button-facebook").html("");
        $(".friendsbox").html(listaAmigos);

    });

};

function insertUser (id, name)
{
    if(users == "")
    {
        users = id;
    }else
    {
        users = users + "," + id;
    }
    
    //Change color when select an user
    $("#elements li#" + id).html("<li id =\""+ 
            id + "\" class=\"element-li1\"><a class=\"element-select\">" + name + "</a> </li>");
    
    //Update Create event button
    $("#lookfor-event").html("<a class=\"enlace\" onclick=\"create_event('" + $("#nameEvent").val() + 
            "','" + users + "','" + $("#description").val() + 
            "','" + $("#venue").val() + "','" + $("#date-hour").val() + 
            "');\">Crear Evento</a>");
}

function create_event(nameEvent,userInvited,descriptionE,venue)
{
    var eventData = {
        access_token: token,
        name:nameEvent,
        start_time:"2013-11-05",
        description:descriptionE,
        location:venue
    };
    
    FB.api("/me/events",'POST',eventData,function(response){

        if(response.id){
            alert("El evento se ha creado correctamente.");
            
            eventDataInvite = {
                users:userInvited
            };
            
            FB.api(response.id + "/invited",'POST',eventDataInvite,function(responseInvite){
                
            });
        }
    });
}

function get_Permissions() {
    FB.api("/1477860192/permissions",'get',function(response){
    if(response.id){
        alert("We have successfully created a Facebook event with ID: "+response.id);
    }
    });
}