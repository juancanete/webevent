/* Function to use into MainPage.html*/


//Globals variables
var clientID = "KURJEQMG13UB4RZRLOVZPRHEFZIDZLOKEVNLOKJBXR0EM5RC";
var clientSecret = "CNYJWZ0XC4R3KRKBBQITZINO0EUJ3GLZTAUVX21TTK0A1NBR";
var gCategory;
var gPosition;
var ge;
var placemarks = [];
var placemark;
var places = [];

google.load("earth", "1");

$(document).ready(function(){
        $("ul.sf-menu").superfish();
    });

function getVenues(categoryId)
{
    this.gCategory = categoryId;

    //First, get current location
    if(navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(setPositionIntoURLFoursquare);
    }else
    {
        alert("No se puede obtener la localización del usuario");

    }
}

/*Parser JSON Data*/
function parserData(url)
{

    $.getJSON(url,function(json){
        var textList = "<ul id=\"elements\">";

        $.each(json.response.venues, function (key,val){

            if(key%2 == 0)
            {
                textList = textList.concat("<li class=\"element-li1\"><a onclick=\"moveCamera("+val.location.lat + "," +  val.location.lng +");\">" + val.name + "</a></li>");
            }else
            {
                textList = textList.concat("<li class=\"element-li2\"><a onclick=\"moveCamera("+val.location.lat + "," +  val.location.lng +");\">" + val.name + "</a></li>");
            }
            
            //Add placemark to google earth
            places.push(val);
            
         });
                textList = textList.concat("</ul>");

                $(".venues").html(textList);
                
                for (place in places)
                {
                    createPlacemark('result', places[place]);
                }
                
                createPlacemark('userPosition', gPosition);
                moveCamera(gPosition.coords.latitude, gPosition.coords.longitude);
                //initGoogleEarth();
        });
}

function setPositionIntoURLFoursquare(position)
{
    var radius = $("#maxradius").val();
    var urlFS = "https://api.foursquare.com/v2/venues/search?client_id="+ 
            clientID +"&client_secret=" + clientSecret +"&v=20131025&ll=" + 
            position.coords.latitude + ","+ position.coords.longitude+"&intent=checkin&radius=" + radius + "&query="+gCategory; //&categoryId="+gCategory;
    gPosition = position;
    
    $.ajax({
        url:urlFS,
        beforeSend:function(){
            $(".venues").html("<img id=\"img-loading\" src=\"MainPage/images/ajax-loader.gif\" alt=\"Imagen de Carga\">");
            places = [];
            removePlacemark();
        },
        success:function(){
            parserData(urlFS);
        }
    });
}

/* Methods Google Earth*/

function initGoogleEarth()
{
     google.earth.createInstance('map3d', initCallback, failureCallback);
     $("#maxradius").val("500");
}

/*Init layer options layer Google Earth*/
function initCallback(instance) 
{
    ge = instance;
    ge.getWindow().setVisibility(true);
    ge.getNavigationControl().setVisibility(ge.VISIBILITY_AUTO);
    
    //Add layer
    ge.getLayerRoot().enableLayerById(ge.LAYER_ROADS, true);
}

function failureCallback(errorCode) 
{
    alert(errorCode);
}

/*Create PlaceMark into Google Earth*/
function createPlacemark(pMType, place) {

    // create the placemark
    placemark = ge.createPlacemark('');
    placemarks.push(placemark);
    
    // Create style map for placemark
    var icon = ge.createIcon('');
    
    if(pMType === "result"){
            icon.setHref('http://maps.google.com/mapfiles/kml/paddle/red-circle.png');
            var style = ge.createStyle('');
            style.getIconStyle().setIcon(icon);
            placemark.setStyleSelector(style);

            // Create point in the map
            var point = ge.createPoint('');
            point.setLatitude(parseFloat(place.location.lat));
            point.setLongitude(parseFloat(place.location.lng));
            placemark.setGeometry(point);

            placemark.setName(place.name);
            
            //Add the placemark to the earth DOM
            ge.getFeatures().appendChild(placemark);

            //Creando balloon
            google.earth.addEventListener(placemark, 'click', function(event) {

            // prevent the default balloon from popping up
            event.preventDefault();

            var balloon = ge.createHtmlStringBalloon('');
            balloon.setFeature(event.getTarget());
            balloon.setMaxWidth(300);
            
            // Message in the balloon
            place.name = place.name.replace("\""," ");
            balloon.setContentString('<a href="EventPage/CreateEventFace.html?nombre='+place.name+'/">Invitar a amigos a un evento en </a>' + place.name);

            ge.setBalloon(balloon);
            
            });
    } else {
            //Create placemark for current position
            icon.setHref('http://gis.dhss.mo.gov/Website/mobileWIC/images/bluedot.png');
            var style = ge.createStyle('');
            style.getIconStyle().setIcon(icon);
            placemark.setStyleSelector(style);

            // Create point in the map
            var point = ge.createPoint('');
            point.setLatitude(parseFloat(place.coords.latitude));
            point.setLongitude(parseFloat(place.coords.longitude));
            placemark.setGeometry(point);

            placemark.setName("ESTAS AQUI");
            
            //Add the placemark to the earth DOM
            ge.getFeatures().appendChild(placemark);
    }
}

function removePlacemark()
{
    var place;
    
    for (placemark in placemarks)
    {
        place = placemarks[placemark];
        
        ge.getFeatures().removeChild(place);
    }
    
    placemarks = [];
}

/*Zoom into Google Earth*/
function moveCamera(lat, long){

    // Create a new LookAt.
    var lookAt = ge.createLookAt('');

    // Set the position values.
    lookAt.setLatitude(parseFloat(lat));
    lookAt.setLongitude(parseFloat(long));
    lookAt.setRange(1000.0); //default is 0.0

    // Update the view in Google Earth.
    ge.getView().setAbstractView(lookAt);
}